#!/usr/bin/python
"""
Irmak Sirer, 2013

Python Wrapper around the Google API for search

It returns results for a google search.
A result is a dictionary (json) with the following fields:

cacheUrl
content
title
titleNoFormatting
unescapedUrl
url
visibleUrl

"""
import sys
import json
import urllib, requests

GOOGLE_API_URL_TEMPLATE = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s'
class GoogleSearch(object):
    
    api_url_template = GOOGLE_API_URL_TEMPLATE
    proxy_no = 0

    def __init__(self, query, verbose=True):
        self.query = query
        self._ajax_query = urllib.urlencode({'q': self.query})
        self._result_data = None
        self.verbose = verbose

        
    @property
    def result_data(self):
        if self._result_data is None:
            query_url = self.api_url_template % self._ajax_query
            self._result_data = self.hit_the_search_api(query_url)
        return self._result_data
    
    def hit_the_search_api(self, query_url):
            tried_proxies = 0
            while True:
                search_response = requests.get(query_url)
                try:
                    response_dict = json.loads(search_response.text)
                    results = response_dict['responseData']
                except ValueError:
                    msg = 'HTTP %s\n%s' % (search_response.status_code,
                                           search_response.text)
                    raise Exception(msg)

                status = response_dict['responseStatus']
                details = response_dict['responseDetails']
                if status != 200:
                    raise Exception(details)

                return results
        
    def top_result(self):
        """ First hit (what "I'm feeling lucky" would return)"""
        return self.result_data['results'][0]

    def top_results(self):
        """ Top hits (only four by default) """
        return self.result_data['results']

    def top_url(self):
        """ URL of the first hit """
        try:
            return self.top_result()['unescapedUrl']
        except:
            return 'Not found('

    def top_urls(self):
        """ URLs of the top hits """
        get_url = lambda result: result['unescapedUrl']
        return map(get_url, self.top_results())

    def count(self):
        "Number of results"
        return int(self.result_data['cursor']['estimatedResultCount'])



