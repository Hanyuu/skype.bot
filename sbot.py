# -*- coding: utf-8 -*-

import Skype4Py
import time
import re
import requests
import xml.dom.minidom
import xml.etree.ElementTree as ET
from HTMLParser import HTMLParser
from htmlentitydefs import name2codepoint
import xml.dom.minidom as parser
import random
from datetime import datetime
import json
from BeautifulSoup import  BeautifulSoup
from search import GoogleSearch
from markov import *

class SkypeBot(object):
    last_quote = None

    def __init__(self):
        self.skype = Skype4Py.Skype(Events=self)
        self.skype.FriendlyName = "Skype Bot"
        self.skype.Attach()



    def AttachmentStatus(self, status):
        if status == Skype4Py.apiAttachAvailable:
            self.skype.Attach()

    def MessageStatus(self, msg, status):
        if status == Skype4Py.cmsReceived:
            if msg.Chat.Type in (Skype4Py.chatTypeDialog, Skype4Py.chatTypeLegacyDialog, Skype4Py.chatTypeMultiChat):
                add_to_brain(msg.Body,chain_length=10,write_to_file=True)
                for regexp, target in self.commands.items():
                    match = re.match(regexp, msg.Body, re.IGNORECASE)
                    if match is not None and match:
                        reply = target(self,match)
                        if reply:
                            msg.Chat.SendMessage(reply)

    def cmd_bash(self,*args,**kwargs):
        r = requests.get('http://bash.im/random').text.encode('utf-8')
        m = re.search('<div class="text">(.+)<\/div>', r)
        quote = m.group(0).replace('<div class="text">','')\
            .replace('</div>','')\
            .replace('<br>','\n')\
            .replace('&quot;','"')\
            .replace('&lt;','<')\
            .replace('&gt;','>')
        return quote

    def cmd_youtube(self,search='test'):
        query_string = search.group(0).replace('@you ','')
        html_content = requests.get("https://www.youtube.com/results?search_query=%s"%query_string).text.encode('utf-8')
        search_results = re.findall(r'href=\"\/watch\?v=(.{11})', html_content)
        video_id = search_results[random.randrange(0,len(search_results)-1)]
        return "http://www.youtube.com/watch?v=%s"%search_results[0]

    def cmd_google(self,search='test'):
        query_string = search.group(0).replace('@g','')
        gs = GoogleSearch(query_string.encode('utf-8'))
        return gs.top_url()

    def cmd_2chhk(self,board='s'):
        board = board.group(0).replace('@2ch.hk ','')
        h = requests.get("https://2ch.hk/%s/threads.json"%board).text.encode('utf-8')
        threads = json.loads(h)['threads']
        thread = threads[random.randrange(0,len(threads)-1)]['num']
        return 'https://2ch.hk/%s/res/%s.html'%(board,thread)

    def markov(self,question):
        question = question.group(0).replace('@m ','')
        return generate_sentence(question,chain_length=random.randrange(10,20),max_words=20)

    def xvideos(self,query_string):
        query_string = query_string.group(0).replace('@x ','')
        html_content = requests.get("http://www.xvideos.com/?k=%s"%query_string).text.encode('utf-8')
        try:
            refind = re.findall(r'<a href="/([0-9\/A-z]+)">',html_content)[4:]
            return 'http://www.xvideos.com/'+refind[random.randrange(0,len(refind)-1)]
        except:
            return 'No videos found'

    def ponnohub(self,query_string):
        query_string = query_string.group(0).replace('@p ','')
        html_content = requests.get("http://www.pornhub.com/video/search?search=%s"%query_string).text.encode('utf-8')
        refind = re.findall(r'(\/view_video.php\?viewkey=[0-9]+)"',html_content)
        return 'http://www.pornhub.com'+refind[random.randrange(0,len(refind)-1)]

    def wiki(self,query_string):
        query_string = query_string.group(0).replace('@w ','')
        html_content = requests.get("https://ru.wikipedia.org/w/index.php?search=%s"%query_string).text.encode('utf-8')
        refind = re.findall(r'<link rel="canonical" href="https://ru.wikipedia.org/wiki/(.*)"/>',html_content)
        try:
            return 'https://ru.wikipedia.org/wiki/%s'%refind[0]
        except:
            refind = re.findall(r'<a href="/wiki/(.*)" title="(.*)" data-serp-pos="3">',html_content)
            return 'https://ru.wikipedia.org/wiki/%s'%refind[0][0]

    commands = {
        "@bash$": cmd_bash,
        '@you(.*)':cmd_youtube,
        '@g(.*)':cmd_google,
        '@2ch.hk(.*)':cmd_2chhk,
        '@m(.*)':markov,
        '@x(.*)':xvideos,
        '@p(.*)':ponnohub,
        '@w(.*)':wiki

      }

if __name__ == "__main__":
    f = open('./db2.txt','r')
    for msg in f:
        add_to_brain(msg,3)
    print('%s\nloaded'%len(markov))
    bot = SkypeBot()
    while True:
        time.sleep(1.0)

